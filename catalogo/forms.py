from django import forms
from .models import Elemento, TipoMoneda

class ElementoForm(forms.ModelForm):
    class Meta:
        model = Elemento
        fields = fields = ('denominacion', 'tipo', 'foto', 'pais','tipo_moneda')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['tipo_moneda'].queryset = TipoMoneda.objects.none()

        if 'pais' in self.data:
            try:
                pais_id = int(self.data.get('pais'))
                self.fields['tipo_moneda'].queryset = Pais.objects.filter(pais_id=pais_id).order_by('nombre')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            self.fields['tipo_moneda'].queryset = self.instance.pais.tipo_moneda_set.order_by('nombre')